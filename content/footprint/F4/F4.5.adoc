+++
title = "F4.5 Specifying footprint keepout areas"
+++

Footprint keepout area must be drawn using as a 'rule-area (keepout)' when necessary or recommended in manufacturer's datasheet.

The area should be named according to its use (e.g. 'Antenna Keepout'), this name must be used to name the area as text also on the User.Comments layer.

. Select only the appropriate layers.

. Check all basic rules. (`Constrain outline` and `Locked` must be kept unchecked)

. Outline display must be `Fully hatched`


*Example:*

Example showing a area named both in the dialog and on the User.Comments layer

{{< klcimg src="F4.5" title="Rule area properties dialog" >}}